from operator import itemgetter
import copy

def print_screen(board) :
	"Print every iteration of board"
	empty_row = "         "

	print
	print
	print "******************************************"
	
	for a in range(3):
		print "*"+empty_row+"*"+empty_row+"*"+empty_row+"*"+empty_row+"*"
		print "*"+empty_row+"*"+empty_row+"*"+empty_row+"*"+empty_row+"*"
		print "*"+" %0.4f  "%board[a][0]+"*"+" %0.4f  "%board[a][1]+"*"+" %0.4f  "%board[a][2]+"*"+" %0.4f  "%board[a][3]+"*"
		print "*"+empty_row+"*"+empty_row+"*"+empty_row+"*"+empty_row+"*"
		print "*"+empty_row+"*"+empty_row+"*"+empty_row+"*"+empty_row+"*"
		print "******************************************"

	print
	print
	
def best_path(old_board,cell) :
	"Get the best pathfrom current board state"
	straight = 0.8
	perpendicular = 0.1

	centre = old_board[cell[0]][cell[1]]
								
	if cell[0] == 0 or cell == (2,2):
		north = centre
	else :
		north = old_board[cell[0] - 1][cell[1]]

	if cell[1] == 3 :
		east = centre
	else :
		east = old_board[cell[0]][cell[1]+1]

	if cell[1] == 0 or cell == (1,3):
		west = centre
	else :
		west = old_board[cell[0]][cell[1]-1]

	if cell[0] == 2 :
		south = centre
	else :
		south = old_board[cell[0]+1][cell[1]]

	dir_list = []
	
	dir_list.append((straight * north + perpendicular * (east + west),1))

	dir_list.append((straight * west + perpendicular * (north + south),4))

	dir_list.append((straight * east + perpendicular * (north + south),2))

	dir_list.append((straight * south + perpendicular * (east + west),3))

	return max(dir_list,key=itemgetter(0))
		
def iterate(board,states) :
	old_board = copy.deepcopy(board)
	reward = -9/20.0
	delta = 0
	
	for cell in states :
		move_pair = best_path(old_board,cell)
		
		board[cell[0]][cell[1]] = reward + move_pair[0]
		
		if (board[cell[0]][cell[1]] - old_board[cell[0]][cell[1]]) > delta :
			delta = board[cell[0]][cell[1]] - old_board[cell[0]][cell[1]]
			
	return delta
			
if __name__ == '__main__':
	board = [[0]*4 for i in range(3)] #initialize board with 0
	board[0][2] = 9
	board[1][1] = -9
	iteration_count = 0
	util_states = [(0,0),(0,1),(0,3),(1,0),(1,3),(2,0),(2,1),(2,2),(2,3)] #All possible moves that can be made
	terminal_states = [(0,2),(1,1)] #All terminal states
	max_diff = 0
	cutoff = 9/20.0 #Mention cutoff if any. Else will iterate till infinity
	print cutoff
	
	while 1:
		print iteration_count
		print_screen(board)
		max_diff = iterate(board,util_states)
		print max_diff
		print cutoff
		if max_diff < cutoff :
			break
		iteration_count += 1

	max_diff = iterate(board,util_states)
	cell = (2,0)
	path = []
	while cell not in terminal_states :
		move_pair = best_path(board,cell)
		print move_pair
		path.append(move_pair[1])
		
		if move_pair[1] == 1 : #north
			cell = (cell[0]-1,cell[1])
		elif move_pair[1] == 2 : #east
			cell = (cell[0],cell[1]+1)
		elif move_pair[1] == 3 : #south
			cell = (cell[0]+1,cell[1])
		elif move_pair[1] == 4 : #west
			cell = (cell[0],cell[1]-1)
					
		print cell
		
	print path
